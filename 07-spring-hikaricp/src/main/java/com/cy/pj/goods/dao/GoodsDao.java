package com.cy.pj.goods.dao;
import java.util.List;
import java.util.Map;
/**
 * 商品的数据访问层接口
 */
public interface GoodsDao {
    /**
     * 从数据库中查询所有商品信息,一行记录
     * 封装到一个map对象(行映射),多行记录存储到list集合
     * @return
     */
    List<Map<String,Object>> findGoods();
}
