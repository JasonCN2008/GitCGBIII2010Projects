package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheTests {
    //has a (是耦合吗?-是)
    //@Autowired
    //@Qualifier("softCache") //需要配置Autowire注解使用,不能单独使用,用于指定要注入的bean的名字
    private Cache cache;//耦合与抽象
    @Test
    void testCache(){
        System.out.println(cache);
    }
}

//MyBatis框架设计时是耦合了JDBC规范还是耦合了JDBC规范的实现(驱动程序)
