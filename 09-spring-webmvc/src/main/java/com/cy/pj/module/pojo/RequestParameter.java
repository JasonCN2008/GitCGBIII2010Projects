package com.cy.pj.module.pojo;

/**
 * pojo对象
 * 通过此对象封装请求参数
 */
public class RequestParameter {

    private String name;
    //............

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RequestParameter{" +
                "name='" + name + '\'' +
                '}';
    }
}
