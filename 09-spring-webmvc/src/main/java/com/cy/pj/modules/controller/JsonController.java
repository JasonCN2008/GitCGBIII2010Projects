package com.cy.pj.modules.controller;


import com.cy.pj.module.pojo.ResponseResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

//@Controller
//@ResponseBody
@RestController //==@Controller+@ResponseBody
public class JsonController {
    /**
     * 假如@ResponseBody注解描述的方法,它的返回值是一个pojo对象,底层在将其转换为
     * json格式字符串时,会默认调用pojo对象的get方法,会使用get方法名中get单词后面的名字
     * 作为key(首字母小写),get方法的返回值作为value,拼接json格式字符串.
     * @return
     */
    @RequestMapping("/doConvertResponseToJson")
    //@ResponseBody
    public ResponseResult doConvertResponseToJson(){
        ResponseResult result=new ResponseResult();
        result.setCode(200);
        result.setMessage("OK");
        return result;
    }

    /**
     * http://localhost/doConvertMapToJson
     * 当使用@ResponseBody注解描述控制层的handler方法时,假如此方法的返回值
     * 为map或pojo这样对象,系统底层会将这样的转换转换为json格式字符串,然后响应到客户端.
     * 思考?
     * 1)在当前应用中这个json格式的字符串会写到http协议的哪一部分中,然后响应到客户端?(响应体)
     * 2)谁将这个map转换为了json格式字符串呢?springboot工程默认使用的是jackson (这组依赖
     * 是我们项目中添加spring web依赖时自动添加的)
     */
      @RequestMapping("/doConvertMapToJson")
      //@ResponseBody
      public Map<String,Object> doConvertMapToJson(){
          Map<String,Object> map=new HashMap<>();
          map.put("state", 1);
          map.put("message", "ok");
          return map;
      }

      @RequestMapping("/doPrintJsonToClient01")
      //@ResponseBody
      public String doPrintJsonToClient01() throws JsonProcessingException {
        Map<String,Object> map=new HashMap<>();
        map.put("state", 1);
        map.put("message", "ok");
        //将map手动转换为json字符串
        ObjectMapper objectMapper=new ObjectMapper();
        String jsonStr=objectMapper.writeValueAsString(map);
        System.out.println("jsonStr="+jsonStr);
        //将这个json字符串响应到客户端
        return jsonStr;//当方法使用了 @ResponseBody注解描述,则方法的返回值就不再是view了
      }
    @RequestMapping("/doPrintJsonToClient02")
    //@ResponseBody
    public void doPrintJsonToClient02(HttpServletResponse response) throws IOException {
          //看过去(最早都是我们自己拼接字符串为json格式)
          //String jsonStr="{\"state\":1,\"message\":\"成功\"}";
        //进阶过程
        Map<String,Object> map=new HashMap<>();
        map.put("state", 1);
        map.put("message", "成功");
        //将map手动转换为json字符串
        ObjectMapper objectMapper=new ObjectMapper();
        String jsonStr=objectMapper.writeValueAsString(map);
        System.out.println("jsonStr="+jsonStr);
        //将这个json字符串响应到客户端
        response.setCharacterEncoding("utf-8");//设置响应数据的编码
        response.setContentType("text/json;charset=utf-8");//告诉客户端响应数据的类型以及编码
        PrintWriter out=response.getWriter();
        out.println(jsonStr);
        out.flush();
    }



}
