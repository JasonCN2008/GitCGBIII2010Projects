package com.cy.pj.modules.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ModelViewController {
    @RequestMapping("/doTemplateModelAndView01")
    public String doTemplateModelAndView(Model model){//model底层是一个map
        //model是一个view中要呈现的数据的一个对象
        model.addAttribute("msg", "html template page");
        return "default";
    }
    @RequestMapping("/doTemplateModelAndView02")
    public ModelAndView doTemplateModelAndView(ModelAndView mv){
        mv.addObject("msg", "The Model and View Demo");
        mv.setViewName("default");
        return mv;
    }
}

