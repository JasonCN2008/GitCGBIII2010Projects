package com.cy.pj.modules.controller;

import com.cy.pj.module.pojo.RequestParameter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 在此对象中演示请求中参数的处理过程
 */
@RestController
public class ParamObjectController {
       //访问:http://localhost/doParam01?id=10
       @RequestMapping("/doParam01")
       public String doMethodParam(HttpServletRequest request){
            String idStr=request.getParameter("id");
            //假如我们后面业务需要的是一个整数,此时还需要进行类型转换
            Integer id=Integer.parseInt(idStr);
            //后续可以基于整数id值执行后面的业务了,例如删除,查找...
            return "request params id's value is "+id;
       }

       //访问:http://localhost/doParam02?id=20
      //当使用方法参数直接获取请求数据时,方法参数名要与请求参数名相同
      @GetMapping("/doParam02")
      public String doMethodParam(Integer id){//框架的力量
        return "request params id's value is "+id;
      }
    //访问:http://localhost/doParam03?ids=1,2,3,4
     @RequestMapping("/doParam03")
     public String doMethodParam(Integer[] ids){//框架的力量
           //....
        return "request params ids's value is "+ Arrays.toString(ids);
     }

    //访问:http://localhost/doParam04?startTime=2020/12/31
    //假如方法要接收日期格式对象可以通过@DateTimeFormat注解指定可接收的日期格式
    //默认格式为yyyy/MM/dd
    @RequestMapping("/doParam04")
    public String doMethodParam(
            @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime){//框架的力量
        //....
        return "request params startTime's value is "+ startTime;
    }
    //访问:http://localhost/doParam05?name=tony&page=2
    //假如在方法参数中需要指定某个参数的值必须在请求参数中有传递
    //@RequestParam注解用于描述方法参数,用于定义参数规则
    //1)方法参数变量的值,来自哪个请求参数,例如@RequestParam("username")
    //2)方法参数变量是否要必须传值 例如@RequestParam(required = true)
    //@RequestMapping(value="/doParam05",method = RequestMethod.GET)
    @GetMapping("/doParam05")
    public String doMethodParam(
            @RequestParam(value="name",required = false) String name,
                      @RequestParam(required = true) Integer page){
        return "request params 's value is "+ name+":"+page;
    }
    //====================POJO对象方式=============================
    //http://localhost/doParam06?name=tony
    //当使用pojo对象封装请求参数信息时,请求中的参数名应与方法参数pojo对象
    //中的set相关方法相匹配
    @GetMapping("/doParam06")
    public String doMethodParam(RequestParameter param){
        return "request params 's value is "+param.toString();
    }

    //@GetMapping("/doParam07")//http://localhost/doParam07?name=tony&ids=1,2
    //post请求可以借助postman工具进行访问
    @PostMapping("/doParam07")//http://localhost/doParam07
    public String doMethodParam(RequestParameter param,Integer... ids){
        return "request params 's value is "+param.toString()+",ids="+Arrays.toString(ids);
    }


    //====================Map对象方式=======================
    //http://localhost/doParam08&code=1&message=ok 假如以这样方式直接在浏览器地址栏访问会有405异常
    //post请求可以借助postman工具进行访问
    //使用map作为方法参数封装请求数据,默认是不可以,因为默认底层会认为这个map是用于封装响应数据的.
    //可以借助@RequestParam对map进行描述,来接收请求参数
    @PostMapping("/doParam08")
    public String doMethodParam(@RequestParam Map<String,Object> map){
        return "request params 's value is "+map.toString();
    }

    /**
     * @RequestBody注解描述方法参数时,客户端
     * 可以以post方式提交json格式的数据,
     * 说明:@RequestBody注解描述的方法参数不能封装Get请求数据
     * @param map
     * @return
     */
    @PostMapping("/doParam09")
    public String doMethodJsonParam(@RequestBody Map<String,Object> map){
        return "request params 's value is "+map.toString();
    }

    @PostMapping("/doParam10")//http://localhost/doParam10
    public String doMethodJsonParam(@RequestBody RequestParameter param){
        return "request params 's value is "+param.toString();
    }
    //=========================rest 风格url参数获取===================
    //rest风格:软件架构编码风格(跨平台)
    //rest风格url的定义:/path/{var1}/{var2},这里的{}括起来的为变量
    //@PathVariable 注解描述方法参数变量时,表示这个参数的值来自url中{}表达式给定值
    //例如:
    @GetMapping("/doParam10/{id}/{name}")
    public String doMethodRestUrlParam(
            @PathVariable  Integer id,@PathVariable  String name){
        return "request params 's value is id=" + id+",name="+name;
    }

}
