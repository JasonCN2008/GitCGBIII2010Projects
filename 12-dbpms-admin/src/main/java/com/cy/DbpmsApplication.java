package com.cy;

import com.cy.pj.sys.service.realm.ShiroRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.LinkedHashMap;

@EnableAsync
@SpringBootApplication
public class DbpmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbpmsApplication.class, args);
    }
    @Bean
    public Realm realm() {//org.apache.shiro.realm.Realm
       return new ShiroRealm();
    }

    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
        LinkedHashMap<String,String> map=new LinkedHashMap<>();
        //设置允许匿名访问的资源路径(不需要登录即可访问)
        map.put("/bower_components/**","anon");//anon对应shiro中的一个匿名过滤器
        map.put("/build/**","anon");
        map.put("/dist/**","anon");
        map.put("/plugins/**","anon");
        map.put("/user/doLogin","anon");
        map.put("/doLogout","logout");
        //设置需认证以后才可以访问的资源(注意这里的顺序,匿名访问资源放在上面)
        map.put("/**","authc");//authc 对应一个认证过滤器，表示认证以后才可以访问
        chainDefinition.addPathDefinitions(map);
        return chainDefinition;
    }

}
