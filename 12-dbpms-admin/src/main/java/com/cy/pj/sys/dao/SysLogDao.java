package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysLogDao {
    int insertObject(SysLog entity);
    /**
     * 早期mybatis版本相对比较时,方法参数有多个或者个方法参数
     * 使用在了动态sql中,需要使用@Param注解对参数进行描述
     * @param username
     * @return
     */
    int getRowCount(@Param("username") String username);
    List<SysLog> findPageObjects(
            @Param("username") String username,
            @Param("startIndex") Integer startIndex,
            @Param("pageSize")Integer pageSize);
}
