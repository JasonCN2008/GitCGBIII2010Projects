package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.dao.SysRoleDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleDao sysRoleDao;
    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    public List<CheckBox> findRoles(){
        //....
        return sysRoleDao.findRoles();
    }
    public SysRoleMenu findById(Integer id){
        SysRoleMenu roleMenu=sysRoleDao.findById(id);//id,name,note
        if(roleMenu==null)
            throw new ServiceException("记录可能已经不存在");
        //List<Integer> menuIds=sysRoleMenuDao.findMenuIdsByRoleId(id);
        //roleMenu.setMenuIds(menuIds);
        return roleMenu;
    }

    @Override
    public int updateObject(SysRole entity, Integer[] menuIds) {
        //1.参数校验(自己校验)
        //2.保存角色自身信息
        int rows=sysRoleDao.updateObject(entity);
        if(rows==0)
            throw new ServiceException("记录可能已经不存在");
        //3.更新角色和菜单关系数据
        sysRoleMenuDao.deleteObjectsByRoleId(entity.getId());
        //System.out.println("menuIds="+Arrays.toString(menuIds));
        sysRoleMenuDao.insertObjects(entity.getId(),menuIds);
        return rows;
    }   @Override
    public int saveObject(SysRole entity, Integer[] menuIds) {
        //1.参数校验(自己校验)
        //2.保存角色自身信息
        int rows=sysRoleDao.insertObject(entity);
        //3.保存角色和菜单关系数据
        sysRoleMenuDao.insertObjects(entity.getId(),menuIds);
        return rows;
    }

    @Override
    public PageObject<SysRole> findPageObjects(String username, Integer pageCurrent) {
        //1.对参数进行校验(可以自己校验，也可以借助框架：spring validation)
        if(pageCurrent==null||pageCurrent<1)
            throw new IllegalArgumentException("页码值不合法");//无效参数异常
        //2.基于查询条件查询总记录数并校验
        int rowCount=sysRoleDao.getRowCount(username);
        if(rowCount==0)
            throw new ServiceException("没有找到对应记录");
        //3.查询当前页记录
        int pageSize=2;//页面大小，每页最多显示多少条记录
        int startIndex=(pageCurrent-1)*pageSize;//当前页起始位置
        List<SysRole> records=
                sysRoleDao.findPageObjects(username,startIndex,pageSize);
        //4.封装查询结果
        return new PageObject<>(rowCount, records, pageSize, pageCurrent);
    }
}
