package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;

import java.util.List;

public interface SysMenuService {
    List<SysUserMenu> findUserMenus(Integer userId);
    public List<Node> findZtreeMenuNodes();
    int saveObject(SysMenu entity);
    int updateObject(SysMenu entity);
    List<SysMenu> findObjects();
}
