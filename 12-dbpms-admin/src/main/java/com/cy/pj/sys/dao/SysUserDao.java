package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SysUserDao {
    @Select("select * from sys_users where username=#{username}")
    SysUser findUserByUsername(String username);
    SysUser findById(Integer id);
    int insertObject(SysUser entity);
    int updateObject(SysUser entity);

    @Update("update sys_users set valid=#{valid},modifiedTime=now() where id=#{id}")
    int validById(Integer id,Integer valid);
    List<SysUser> findPageObjects(String username);
}
