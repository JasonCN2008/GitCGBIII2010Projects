package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysRoleDao {
    @Select("select id,name from sys_roles")
    List<CheckBox> findRoles();

    int updateObject(SysRole entity);
    SysRoleMenu findById(Integer id);
    int insertObject(SysRole entity);
    int getRowCount(String name);
    List<SysRole> findPageObjects(String name,
                                  Integer startIndex,
                                  Integer pageSize);
}
