package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysUser;

import java.util.Map;

public interface SysUserService {
    int updateObject(SysUser entity,Integer[]roleIds);
    Map<String,Object> findById(Integer id);
    int saveObject(SysUser entity,Integer[]roleIds);
    int validById(Integer id,Integer valid);
    PageObject<SysUser> findPageObjects(String username,
                                        Integer pageCurrent);
}
