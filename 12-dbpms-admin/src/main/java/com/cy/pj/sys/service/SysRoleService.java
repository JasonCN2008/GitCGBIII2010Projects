package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;

import java.util.List;

public interface SysRoleService {
    List<CheckBox> findRoles();
    SysRoleMenu findById(Integer id);
    int saveObject(SysRole entity,Integer[] menuIds);
    int updateObject(SysRole entity,Integer[] menuIds);
    /**
     * 基于条件查询用户行为日志并进行封装
     * @param name 查询条件
     * @param pageCurrent 当前页面值
     * @return 封装分页查询结果
     */
    PageObject<SysRole> findPageObjects(String name,
                                        Integer pageCurrent);
}
