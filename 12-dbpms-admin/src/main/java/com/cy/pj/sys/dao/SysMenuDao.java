package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

@Mapper
public interface SysMenuDao{
    List<SysUserMenu> findUserMenus(Integer userId);
    Set<String> findUserPermissions(Integer userId);

    @Select("select id,name,parentId from sys_menus")
    public List<Node> findZtreeMenuNodes();

    int updateObject(SysMenu entity);
    int insertObject(SysMenu entity);
    /**查询所有菜单信息*/
    List<SysMenu> findObjects();
}