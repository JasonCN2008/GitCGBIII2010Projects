package com.cy.pj.sys.service.impl;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuDao sysMenuDao;


    public List<SysUserMenu> findUserMenus(Integer userId){
        return sysMenuDao.findUserMenus(userId);
    }

    @Override
    public List<Node> findZtreeMenuNodes() {
        return sysMenuDao.findZtreeMenuNodes();
    }

    @Override
    public int saveObject(SysMenu entity) {
        //.....
        return sysMenuDao.insertObject(entity);
    }    @Override
    public int updateObject(SysMenu entity) {
        //.....
        return sysMenuDao.updateObject(entity);
    }
    public List<SysMenu> findObjects(){
        return sysMenuDao.findObjects();
    }
}
