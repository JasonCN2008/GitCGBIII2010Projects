package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysUserMenu implements Serializable {
    private static final long serialVersionUID = -410105494012229800L;
    private Integer id;
    private String name;
    private String url;
    private List<SysUserMenu> childMenus;
}
