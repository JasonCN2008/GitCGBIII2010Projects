package com.cy;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;
import java.util.UUID;

@SpringBootTest
class ApplicationTests {

    @Test
    void testMd501() {
        String password="123456";
        SimpleHash simpleHash=
        new SimpleHash("MD5", password);
        System.out.println(simpleHash.toHex());
    }
    @Test
    void testMd502() {
        String password="123456";
        String salt= UUID.randomUUID().toString();
        System.out.println("salt="+salt);
        SimpleHash simpleHash=
                new SimpleHash("MD5", password,salt,1);
        System.out.println(simpleHash.toHex());
        //e9f5ed4bc907304e63a97306cd2e2e30

    }
    @Test
    void testMd503() {
        String password="123456";
        String salt= "c858fdf0-27d7-444b-bb60-de8efc5e2cb8";
        SimpleHash simpleHash=
                new SimpleHash("MD5", password,salt,1);
        System.out.println(simpleHash.toHex());//e9f5ed4bc907304e63a97306cd2e2e30
    }

    @Test
    void testBase64(){//可逆加密算法
        String password="123456";
        //获取加密对象Encoder,对密码进行加密
        Base64.Encoder encoder=Base64.getEncoder();
        String hashedPassword=encoder.encodeToString(password.getBytes());
        System.out.println(hashedPassword);//MTIzNDU2
        //获取解密对象decoder,对密码进行解码
        Base64.Decoder decoder = Base64.getDecoder();
        password=new String(decoder.decode(hashedPassword));
        System.out.println(password);//"123456"
    }

}
