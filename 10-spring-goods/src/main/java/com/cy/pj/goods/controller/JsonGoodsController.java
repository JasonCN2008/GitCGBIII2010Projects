package com.cy.pj.goods.controller;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/goods/")//定义url中的共性部分
public class JsonGoodsController {
    @Autowired
    private GoodsService goodsService;

    //基于条件查询商品信息,并将信息转换为json串返回
    @GetMapping("doFindJsonGoods")
    //@ResponseBody
    public JsonResult doFindJsonGoods(String name){
       List<Goods> list=goodsService.findGoods(name);
       return new JsonResult(list);
    }

   // @ResponseBody
    @PostMapping("doSaveJsonGoods")
    public JsonResult doSaveGoods(@RequestBody  Goods goods){
        goodsService.saveGoods(goods);
        return new JsonResult("save ok");
    }
    //.....

}
