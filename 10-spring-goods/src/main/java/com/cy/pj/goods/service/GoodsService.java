package com.cy.pj.goods.service;

import com.cy.pj.goods.pojo.Goods;

import java.util.List;

public interface GoodsService {
    int updateGoods(Goods goods);
    Goods findById(Integer id);
    int saveGoods(Goods goods);
    int deleteById(Integer id);
    List<Goods> findGoods(String name);
}
