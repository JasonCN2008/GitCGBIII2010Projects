package com.cy.pj.goods.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@AllArgsConstructor
@NoArgsConstructor //无参构造函数
//@Setter
//@Getter
//@ToString
@Data //此注解描述类时,在编译的时会为此类生成set,get,toString,hashCode,equals等方法
public class Goods {//此类在编译成.class文件时,lombok会执行字节码操作.
    private Integer id;
    private String name;
    private String remark;
    //alt+enter 引入类
    private Date createdTime;
    //alt+insert 添加方法
}
//思考:
//1.源代码中没有的方法,在class文件中一定没有吗?不一定
//2.所有的.class文件一定会有对应的源码吗?不一定
