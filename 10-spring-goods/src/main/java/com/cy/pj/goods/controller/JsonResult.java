package com.cy.pj.goods.controller;

import lombok.Data;

/**
 * 通过此类封装从服务端响应到客户端的数据信息,这些信息一般包括
 * 1)状态码
 * 2)状态码对应的消息
 * 3)具体数据
 */
@Data
public class JsonResult {
    private Integer state=1;//状态码 ,1表示ok,0表示error
    private String message="ok";//状态信息
    private Object data;//一般用于封装查询结果
    public JsonResult(String message){//new JsonResult("save ok");
        this.message=message;
    }
    public JsonResult(Object data){//new JsonResult(List<?> object)
        this.data=data;
    }
    public JsonResult(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }
}
