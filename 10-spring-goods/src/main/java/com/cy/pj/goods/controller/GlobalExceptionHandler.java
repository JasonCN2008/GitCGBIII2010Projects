package com.cy.pj.goods.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
//@ResponseBody
//@ControllerAdvice //此注解描述的类为全局异常处理类,在此类中可以定义多个异常处理方法
@RestControllerAdvice //@ControllerAdvice+@ResponseBody
public class GlobalExceptionHandler {
    //异常处理方法
//    @ExceptionHandler(IllegalArgumentException.class)
//    @ResponseBody
//    public String doHandleException(IllegalArgumentException e){
//        log.error("IllegalArgumentException.exception {}",e.getMessage());
//        return e.getMessage();
//    }
    //异常处理方法
//    @ExceptionHandler(RuntimeException.class)
//    //@ResponseBody
//    public String doHandleException(RuntimeException e){
//        log.error("RuntimeException.exception {}",e.getMessage());
//        return e.getMessage();
//    }

    @ExceptionHandler(RuntimeException.class)
    //@ResponseBody
    public JsonResult doHandleException(RuntimeException e){
        log.error("RuntimeException.exception {}",e.getMessage());
        return new JsonResult(e);
    }
}
//思考:假如将来多个项目都需要有这样的一个异常类,你会怎么办?
