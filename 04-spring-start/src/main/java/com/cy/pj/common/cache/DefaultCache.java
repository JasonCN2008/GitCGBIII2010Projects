package com.cy.pj.common.cache;

import org.springframework.stereotype.Component;

/**
 * @Component 为spring框架中定义的用于描述一般bean对象的
 * 一个注解.
 * SpringBoot工程在启动时会自动扫描启动类所在包以及子包中
 * 的类,并读取这些类的描述,假如这些类使用了类似@Component
 * 这样的注解描述,它会交给spring管理.
 */
@Component
public class DefaultCache {
}
