package com.cy;

public class MethodTests {
    //main方法会运行主线程,会是主线程方法栈的栈底元素
    public static void main(String[] args) {
        doMethod01();//方法调用时会执行入栈操作,方法执行结束会执行出栈操作
    }
    static void doMethod01(){
        doMethod02();
    }
    static void doMethod02(){
        //获取当前线程的方法栈信息(每个线程都有一个私有的方法栈)
        //每个调用方法的相关信息会以StackTrace(栈帧-方法名,参数,返回值)的形式进入栈内存
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        System.out.println(stackTrace[1].getMethodName());
//        for(StackTraceElement e:stackTrace){
//            System.out.println(e.getMethodName());
//        }
    }
}
