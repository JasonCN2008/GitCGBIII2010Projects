package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品模块数据逻辑接口定义,在此接口中
 * 定义商品数据的操作逻辑
 * @Mapper注解由Mybatis提供,用于描述数据逻辑层接口,由mybatis底层为接口创建其实现类
 * ,并将其实现类的对象交给Spring管理,在这个实现类的内部会基于sqlsession对象实现与
 * 数据库的会话
 */
@Mapper
public interface GoodsDao {
    List<Goods> findGoods();
}

