package com.cy.pj.goods.service.impl;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品业务逻辑对象，此对象中要实现：
 * 1)核心业务
 * 2)拓展业务(日志，缓存，校验，权限，事务,.....)
 */
@Service
public class GoodsServiceImpl implements GoodsService {
    //获取一个日志对象(推荐使用slf4j-simple logging facade for java包中的日志对象-因为这是规范)
    private static final Logger log=//org.slf4j.Logger  (支持log4j,logback)
            LoggerFactory.getLogger(GoodsServiceImpl.class);//org.slf4j.LoggerFactory
    @Autowired
    private GoodsDao goodsDao;
    @Override
    public List<Goods> findGoods() {
        long t1=System.currentTimeMillis();
        //System.out.println("start "+t1);
        log.info("start: {} ",t1);//这里的{}表示占位符
        List<Goods> list = goodsDao.findGoods();
        long t2=System.currentTimeMillis();
        //System.out.println("end: "+t2);
        log.info("end: {} ",t2);
        return list;
    }
}
