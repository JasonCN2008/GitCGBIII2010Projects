package com.cy.pj.goods.dao.impl;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//@Repository
public class GoodsDaoImpl implements GoodsDao {
    @Autowired
    private SqlSession sqlSession;

    @Override
    public List<Goods> findGoods() {
        //mybatis数据访问实现.
        //String namespace="com.cy.pj.goods.dao.GoodsDao";
        String namespace=this.getClass().getInterfaces()[0].getName();
        System.out.println("namespace="+namespace);
        //String elementId="findGoods";
        String elementId=Thread.currentThread().getStackTrace()[1].getMethodName();
        //System.out.println("elementId="+elementId);
        String statement=namespace+"."+elementId;//映射的namespace+elementId
        List<Goods> list=sqlSession.selectList(statement);
        //在通过sqlSession执行会话时,会基于statement(key)从Map<String,MappedStatement>中
        //查找对应的MappedStatement对象,然后基于MappedStatement对象中封装的sql信息执行SQL会话
        return list;
    }
}
