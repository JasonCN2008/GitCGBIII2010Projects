package com.cy;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogTests {
    private static final Logger log= LoggerFactory.getLogger(LogTests.class);

    @Test
    void testLogLevel(){//日志级别 trace<debug<info<error
         log.trace("==trace==");
         log.debug("==debug==");
         log.info("==info==");
         log.error("==error==");
    }
}
