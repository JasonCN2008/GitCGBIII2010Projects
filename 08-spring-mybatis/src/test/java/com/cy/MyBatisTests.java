package com.cy;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;

@SpringBootTest
public class MyBatisTests {
    /**
     * 此对象是mybatis框架与数据库进行会话的入口对象.
     */
    @Autowired
    private SqlSession sqlSession;

    @Test
    void testGetConnection(){
        //这个连接来自哪里?连接池
        Connection conn=
        sqlSession.getConnection();
        System.out.println(conn);
    }
}
