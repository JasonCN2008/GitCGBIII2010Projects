package com.cy;
import com.cy.pj.common.pool.ObjectPool;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class FeaturesApplication {

    @Bean
    public ObjectPool oPool(){
        return new ObjectPool();
    }
    public static void main(String[] args) {
        SpringApplication.run(FeaturesApplication.class, args);
    }

}
